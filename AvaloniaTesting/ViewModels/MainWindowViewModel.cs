﻿using System.Windows.Input;
using Avalonia;
using AvaloniaTesting.Utils;

namespace AvaloniaTesting.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public string Greeting => "Hello World!";
        public MainWindowViewModel()
        {
            PreviousCommand = new RelayCommand(Previous);
            PlayCommand = new RelayCommand(Play);
            NextCommand = new RelayCommand(Next);
            StopCommand = new RelayCommand(Stop);
        }

        public double ScrollingTextMargin { get; set; } = 50.0;
        public string Message { get; set; } = "Testing";

        public ICommand PreviousCommand { get; }

        private void Previous()
        {
            //throw new NotImplementedException();
        }

        public ICommand PlayCommand { get; }

        private void Play()
        {
            //throw new NotImplementedException();
        }

        public ICommand NextCommand { get; }

        private void Next()
        {
            //throw new NotImplementedException();
        }

        public ICommand StopCommand { get; }

        private void Stop()
        {
            //throw new NotImplementedException();
            Application.Current.Exit();
        }
    }
}
