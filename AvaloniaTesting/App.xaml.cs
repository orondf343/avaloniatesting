﻿using Avalonia;
using Avalonia.Markup.Xaml;

namespace AvaloniaTesting
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
